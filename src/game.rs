use std::fmt::{Display, Formatter};

const TOTAL_PIN_NUMBER: u8 = 10;
const MAX_ROLL_NUMBER: u8 = 21;
pub struct Game {
    rolls: Vec<u8>,
}

#[derive(Debug)]
pub struct MaxRollsReachedError;

impl Display for MaxRollsReachedError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Max number rolls reached")
    }
}

impl Game {
    pub fn new() -> Self {
        Game {
            rolls: vec![],
        }
    }
    pub fn roll(&mut self, pins_down: u8) -> Result<(), MaxRollsReachedError> {
        if self.rolls.len() > MAX_ROLL_NUMBER as usize {
            return Err(MaxRollsReachedError);
        }
        self.rolls.push(pins_down);
        Ok(())
    }

    pub fn score(&self) -> u16 {
        let mut total_score = 0u16;
        let mut roll_index = 0;

        for _ in 0..10 {
            if is_strike(self.rolls[roll_index]) {
                total_score += (TOTAL_PIN_NUMBER
                    + self.rolls[roll_index + 1]
                    + self.rolls[roll_index + 2]) as u16;
                roll_index += 1;
            } else if is_spare(self.rolls[roll_index], self.rolls[roll_index + 1]) {
                total_score += (TOTAL_PIN_NUMBER + self.rolls[roll_index + 2]) as u16;
                roll_index += 2;
            } else {
                total_score += (self.rolls[roll_index] + self.rolls[roll_index + 1]) as u16;
                roll_index += 2;
            }
        }

        total_score
    }
}

fn is_spare(roll1: u8, roll2: u8) -> bool {
    roll1 + roll2 == TOTAL_PIN_NUMBER
}

fn is_strike(roll: u8) -> bool {
    roll == TOTAL_PIN_NUMBER
}