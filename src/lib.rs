pub mod game;

#[cfg(test)]
mod tests {
    use crate::game::{Game, MaxRollsReachedError};

    #[test]
    fn gutter_game_test() -> Result<(), MaxRollsReachedError> {
        let mut game = Game::new();
        roll_many(&mut game, 20, 0)?;
        assert_eq!(0, game.score());
        Ok(())
    }


    #[test]
    fn too_much_rolls_test() -> Result<(), MaxRollsReachedError> {
        let mut game = Game::new();
        roll_many(&mut game, 30, 0).expect_err("Expected error for too many rolls");
        Ok(())
    }

    #[test]
    fn all_one_test() -> Result<(), MaxRollsReachedError> {
        let mut game = Game::new();
        roll_many(&mut game, 20, 1)?;
        assert_eq!(20, game.score());
        Ok(())
    }

    #[test]
    fn one_spare_test() -> Result<(), MaxRollsReachedError> {
        let mut game = Game::new();
        game.roll(5)?;
        game.roll(5)?;
        game.roll(3)?;
        roll_many(&mut game, 17, 0)?;
        assert_eq!(16, game.score());
        Ok(())
    }

    #[test]
    fn one_strike_test() -> Result<(), MaxRollsReachedError> {
        let mut game = Game::new();
        game.roll(10)?;
        game.roll(3)?;
        game.roll(4)?;
        roll_many(&mut game, 16, 0)?;
        assert_eq!(24, game.score());
        Ok(())
    }

    #[test]
    fn last_strike_test() -> Result<(), MaxRollsReachedError> {
        let mut game = Game::new();
        roll_many(&mut game, 18, 0)?;
        game.roll(10)?;
        game.roll(3)?;
        game.roll(4)?;
        assert_eq!(17, game.score());
        Ok(())
    }

    #[test]
    fn last_spare_test() -> Result<(), MaxRollsReachedError> {
        let mut game = Game::new();
        roll_many(&mut game, 18, 0)?;
        game.roll(5)?;
        game.roll(5)?;
        game.roll(4)?;
        assert_eq!(14, game.score());
        Ok(())
    }

    #[test]
    fn one_spare_and_strike() -> Result<(), MaxRollsReachedError> {
        let mut game = Game::new();
        game.roll(5)?;
        game.roll(5)?;
        game.roll(4)?;
        game.roll(3)?;
        game.roll(10)?;
        game.roll(5)?;
        game.roll(2)?;
        game.roll(7)?;
        game.roll(1)?;
        roll_many(&mut game, 10, 0)?;
        assert_eq!(53, game.score());
        Ok(())
    }

    #[test]
    fn perfect_game_test() -> Result<(), MaxRollsReachedError> {
        let mut game = Game::new();
        roll_many(&mut game, 12, 10)?;
        assert_eq!(300, game.score());
        Ok(())
    }
    fn roll_many(game: &mut Game, times: u8, pins_down: u8) -> Result<(), MaxRollsReachedError> {
        for _ in 0..times {
            game.roll(pins_down)?;
        }
        Ok(())
    }
}