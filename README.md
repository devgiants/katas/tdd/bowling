# Uncle Bob's bowling kata in Rust

This repo aims to provide a bowling kata (from [Uncle Bob](http://butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata)) 
implementation in Rust.

It's one implementation proposal, there are multiple ways to achieve it. Several aims :
- Show the baby-stepping concept : always ensuring we add thin layer. 
- Illustrate the ability to temporarily __rollback/comment__ new tests added, in order to fallback on previous refactoring step.
- Refactoring is not only algorithmic : architectural review is mandatory to keep growing features on a project. 